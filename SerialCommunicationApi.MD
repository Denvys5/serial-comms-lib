# Serial communication API

This software is using low-level binary protocol for sending and receiving messages to/from serial port.

## 1 Serial message structure

Common format of a message is following:
```text
1) marker - 4 byte 
2) messageId - 1 byte 
3) commandId - 1 byte 
4) dataId - 1 byte 
5) argc - 1 byte 
6) argv - argc byte 
7) crc - 2 byte 
```

Message format with byte positions:
```text
1) marker - 1st byte 
2) marker - 2nd byte 
3) marker - 3rd byte 
4) marker - 4th byte 
5) messageId - 1st byte 
6) commandId - 1st byte 
7) dataId - 1st byte 
8) argc - 1st byte 
9+argc) argv - argc byte 
9+argc(if argc >0 do +1)) crc - 1st byte 
10+argc(if argc >0 do +1)) crc - 2nd byte 
```
*that means that if argc=0, we do not skip 1 position for an empty argv*

Message struct:
```text
uint32_t marker;
uint8_t messageId;
uint8_t commandId;
uint8_t dataId;
int8_t argc;
uint8_t[] argv;
uint16_t crc;
```

Sample array:
```text
{marker, marker, marker, marker, messageId, commandId, dataId, argc=0, crc, crc}
```
```text
{marker, marker, marker, marker, messageId, commandId, dataId, argc=1, argv, crc, crc}
```

  * **marker** – marker that identifies start of a message.
  * **messageId** – ID of a request. Can be used to determine future responses.
  * **commandId** – ID of protocol command from enum.
  * **dataId** – ID of data command from enum.
  * **argc** – size of argv array.
  * **argv** – array which carries command arguments. Can be empty if commandId allows it.
  * **crc** – checksum of message (excluding itself).

*Notice, all values being written in hexadecimal*

commandId enum:
* AB **ACK_COMMAND_ID** – acknowledgment of receiving of certain message.
* AC **HANDSHAKE_COMMAND_ID** – handshake command to establish serial communication.
* AD **PING_COMMAND_ID** – ping command. Is used to determine if a connection is still active.
* BB **DATA_COMMAND_ID** – serial data message. Requires dataId field.

## 2 Serial commands in detail

### 2.1 ACK command
Hexadecimal: AB
Decimal: 171

A protocol response to each received message.
```text
{marker, marker, marker, marker, messageId, 0xAB, 0, 0, crc, crc}
```
 * **messageId** - is equal to received message.

### 2.2 Handshake command
Hexadecimal: AC
Decimal: 172

Handshake protocol message to establish communication link.
```text
{marker, marker, marker, marker, messageId, 0xAC, 0, 0, crc, crc}
```

### 2.3 Ping command
Hexadecimal: AD
Decimal: 173

Ping protocol message to verify that communication link is still active.
```text
{marker, marker, marker, marker, messageId, 0xAD, 0, 0, crc, crc}
```
Sent if last message received was N seconds ago.
If there is no answer in N+K seconds, link is considered offline.

### 2.4 Data command
Hexadecimal: BB
Decimal: 187

Data protocol message that indicates that message contains data from higher OSI level.
That data is written in dataId and argv fields of message array.
```text
{marker, marker, marker, marker, messageId, 0xBB, 0, 0, crc, crc}
```

## 3 Error correcting code
This protocol support on-the-fly error correction. 
It is implemented by using CRC-16 checksum at the end of each message.

This particular project utilizes CRC-16-CCITT \
Poly : 0x1021 x^16 + x^12 + x^5 + 1 \
Init : 0x0000 \
Revert: false \
XorOut: 0x00 \
Check : 0x31C3 (for char array "123456789") \
MaxLen: 4095 byte (32767 bit)
