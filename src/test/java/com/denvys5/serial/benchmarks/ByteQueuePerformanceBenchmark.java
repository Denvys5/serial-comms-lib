package com.denvys5.serial.benchmarks;

import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.serial_message.SerialProtocolCommandEnumeration;
import com.denvys5.serial.service.SerialMessageFactory;
import com.denvys5.serial.service.SerialMessageParsingUtils;
import com.denvys5.serial.service.SerialMessageUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ByteQueuePerformanceBenchmark {

    @State(Scope.Thread)
    public static class DefaultReceiverState {
        byte[] inputData;

        @Setup(Level.Trial)
        public void initialize() {
            Random random = new Random();
            SerialMessageFactory serialMessageFactory = new SerialMessageFactory();
            LinkedList<Byte> linkedList = new LinkedList<>();
            byte[] argv;
            for (int i = 0; i < 10000; i++) {
                for (byte value:SerialMessageUtils.getByteArrayFromData(serialMessageFactory.getPingMessage()))
                    linkedList.add(value);
                for (byte value:SerialMessageUtils.getByteArrayFromData(serialMessageFactory.getHandshakeMessage()))
                    linkedList.add(value);
                for (byte value:SerialMessageUtils.getByteArrayFromData(serialMessageFactory.getACKMessage((byte) random.nextInt())))
                    linkedList.add(value);
                argv = new byte[random.nextInt(10)];
                random.nextBytes(argv);
                for (byte value:SerialMessageUtils.getByteArrayFromData(serialMessageFactory.getSerialDataMessageDto((byte) random.nextInt(50), argv)))
                    linkedList.add(value);
            }
            inputData = ArrayUtils.toPrimitive(linkedList.toArray(new Byte[0]));
        }
    }

    @Benchmark
    public void benchmarkReceiver(DefaultReceiverState state, Blackhole bh){
        AbstractMessageReceiverMock receiverMock = new MessageReceiverMock();
        receiverMock.setBlackhole(bh);
        receiverMock.addAllBufferToQueue(state.inputData);
        boolean flag;
        do{
            flag = receiverMock.parseQueue();
        } while(flag);
    }

    private static abstract class AbstractMessageReceiverMock{
        private final SerialMessageFactory serialMessageFactory = new SerialMessageFactory();
        private Blackhole blackhole;

        public void setBlackhole(Blackhole blackhole) {
            this.blackhole = blackhole;
        }

        protected abstract void addAllBufferToQueue(byte[] messageBuffer);
        protected abstract boolean parseQueue();

        protected void processMessage(byte[] messageBuffer) {
            SerialMessageDto serialMessageDto = serialMessageFactory.getMessageFromByteArray(messageBuffer);

            switch (SerialProtocolCommandEnumeration.getCommandById(serialMessageDto.getCommandId())){
                case ACK_COMMAND_ID:
                case DATA_COMMAND_ID:
                case PING_COMMAND_ID:
                case HANDSHAKE_COMMAND_ID: {
                    blackhole.consume(serialMessageDto);
                    break;
                }
                default:
            }
        }
    }
    private static class MessageReceiverMock extends AbstractMessageReceiverMock{
        private final LinkedList<Byte> readQueue = new LinkedList<>();

        public void addAllBufferToQueue(byte[] messageBuffer) {
            synchronized (readQueue) {
                for (byte buffer : messageBuffer)
                    readQueue.add(buffer);
            }
        }
        public boolean parseQueue(){
            synchronized (readQueue){
                int startIndex = readQueue.indexOf(SerialMessageParsingUtils.getSelectByteInMarker(0));
                if(startIndex == -1) return false;

                boolean foundProperlySizedMessage = false;
                boolean foundInvalidMessage = false;
                int arraySize = SerialMessageUtils.getSmallestMessageSize();
                if(checkMarkerByIndex(startIndex)){
                    if(readQueue.size() >= startIndex + SerialMessageUtils.getSmallestMessageSize()){
                        int argc = readQueue.get(SerialMessageParsingUtils.getArgcIndex()) & 0xFF;
                        arraySize = SerialMessageUtils.getSmallestMessageSize() + argc;
                        if(readQueue.size() >= startIndex + arraySize){
                            foundProperlySizedMessage = true;
                            ListIterator<Byte> iterator = readQueue.listIterator(startIndex);
                            byte[] buffer = new byte[arraySize];
                            for(int i = 0; i < buffer.length; i++){
                                buffer[i] = iterator.next();
                            }

                            if(parseByteRange(buffer)){
                                processMessage(buffer);
                            }else foundInvalidMessage = true;
                        }
                    }
                }

                for(int i = 0; i < startIndex; i++){
                    readQueue.removeFirst();
                }

                if(foundInvalidMessage){
                    for(int i = 0; i < Integer.BYTES; i++){
                        readQueue.removeFirst();
                    }
                }else{
                    if(foundProperlySizedMessage){
                        if(readQueue.size() >= arraySize){
                            for(int i = 0; i < arraySize; i++){
                                readQueue.removeFirst();
                            }
                        }else{
                            readQueue.clear();
                        }
                    }
                }
                return true;
            }
        }
        private boolean parseByteRange(byte[] array){
            if(SerialMessageParsingUtils.validateMaskAtArrayStart(array)){
                if(SerialMessageParsingUtils.validateByteArrayAsMessage(array)){
                    if(SerialMessageParsingUtils.validateCrcInByteArray(array)){
                        return true;
                    }
                }
            }
            return false;
        }
        private boolean checkMarkerByIndex(int index){
            ListIterator<Byte> iterator = readQueue.listIterator(index+1);
            if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(1))){
                if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(2))){
                    if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(3))){
                        return true;
                    }
                }
            }
            return false;
        }
    }


    @Test
    public void benchmark() throws Exception {
        Options opt = new OptionsBuilder()
                // Specify which benchmarks to run.
                // You can be more specific if you'd like to run only one benchmark per test.
                .include(this.getClass().getName() + ".*")
                // Set the following options as needed
                .mode(Mode.AverageTime)
                .timeUnit(TimeUnit.MILLISECONDS)
                .warmupTime(TimeValue.milliseconds(100))
                .warmupIterations(3)
                .measurementTime(TimeValue.milliseconds(100))
                .measurementIterations(5)
                .threads(2)
                .forks(1)
                .shouldFailOnError(true)
                .shouldDoGC(true)
//                .jvmArgs("-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintInlining")
                .build();

        new Runner(opt).run();
    }
}
