package com.denvys5.serial.benchmarks;

import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ByteSearchPerformanceBenchmark {

    @State(Scope.Thread)
    public static class ByteSearchState {
        byte[] input;
        byte[] messageIds;
        int messageIdIterator;

        @Setup(Level.Trial)
        public void initialize() {
            input = new byte[1000000];
            new Random().nextBytes(input);
            messageIds = new byte[30];
            messageIdIterator = 0;
        }
    }

    @Benchmark
    public void benchmarkLinearSearch(ByteSearchState state, Blackhole bh){
        AbstractSearchClass searchClass = new LinearSearchClass();
        for (byte value:state.input){
            bh.consume(searchClass.isUnique(value));
        }
    }

    private static abstract class AbstractSearchClass{
        protected final byte[] messageIds = new byte[30];
        protected int messageIdIterator = 0;
        public abstract boolean isUnique(byte newMessageId);
    }

    private static class LinearSearchClass extends AbstractSearchClass{
        public synchronized boolean isUnique(byte newMessageId){
            for (byte messageId : messageIds) {
                if (messageId == newMessageId) {
                    return false;
                }
            }
            messageIds[messageIdIterator++] = newMessageId;
            if(messageIdIterator == messageIds.length)
                messageIdIterator = 0;
            return true;
        }
    }

    @Test
    public void benchmark() throws Exception {
        Options opt = new OptionsBuilder()
                // Specify which benchmarks to run.
                // You can be more specific if you'd like to run only one benchmark per test.
                .include(this.getClass().getName() + ".*")
                // Set the following options as needed
                .mode(Mode.AverageTime)
                .timeUnit(TimeUnit.MILLISECONDS)
                .warmupTime(TimeValue.milliseconds(100))
                .warmupIterations(5)
                .measurementTime(TimeValue.milliseconds(100))
                .measurementIterations(3)
                .threads(4)
                .forks(1)
                .shouldFailOnError(true)
                .shouldDoGC(true)
//                .jvmArgs("-XX:+UnlockDiagnosticVMOptions", "-XX:+PrintInlining")
                .build();

        new Runner(opt).run();
    }
}
