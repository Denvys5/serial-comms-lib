package com.denvys5.serial;

import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.service.*;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_listeners.SerialConnectionListeners;
import com.denvys5.serial.service.serial_port.SerialPortMock;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class SerialPortMockHardwareTest {
    SerialConnector serialConnector;
    SerialPortListener serialPortListener;
    SerialPortListWrapper serialPortListWrapper;
    SerialPacketTransceiver serialPacketTransceiver;
    SerialPacketReceiver serialPacketReceiver;
    SerialMessageFactory serialMessageFactory;
    SerialProtocolMessageListeners serialProtocolMessageListeners;
    SerialConnectionListeners serialConnectionListeners;
    SerialTimeoutService serialTimeoutService;
    SerialConfigController serialConfigController;

    @BeforeEach
    public void setup(){
        serialMessageFactory = new SerialMessageFactory();

        serialProtocolMessageListeners = new SerialProtocolMessageListeners();

        serialConfigController = spy(SerialConfigController.class);
        when(serialConfigController.isSerialDebug()).thenReturn(true);
        when(serialConfigController.getSerialCommunicationSpeed()).thenReturn(9600);
        when(serialConfigController.isSerialRawMessages()).thenReturn(false);
        when(serialConfigController.getSerialConnectionTimeout()).thenReturn(10000);
        when(serialConfigController.getSerialTimeoutInitialDelay()).thenReturn(25000);
        when(serialConfigController.getSerialClearAcksTimeDelay()).thenReturn(2000);
        when(serialConfigController.getSerialSendMessagesTimeDelay()).thenReturn(100);
        when(serialConfigController.getSerialTimeForACK()).thenReturn(1000);
        when(serialConfigController.getSerialPortScanDelay()).thenReturn(500);
        when(serialConfigController.getSerialWaitForDevice()).thenReturn(1000);
        when(serialConfigController.getSerialWaitForDeviceCycles()).thenReturn(4);

        serialTimeoutService = new SerialTimeoutService();
        serialTimeoutService.setConfigController(serialConfigController);

        serialPacketReceiver = new SerialPacketReceiver();
        serialPacketReceiver.setSerialTimeoutService(serialTimeoutService);
        serialPacketReceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketReceiver.setListeners(serialProtocolMessageListeners);
        serialPacketReceiver.setConfigController(serialConfigController);

        serialConnector = spy(SerialConnector.class);
        serialConnector.setConfigController(serialConfigController);
        serialConnector.setSerialPacketReceiver(serialPacketReceiver);
        serialConnector.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        when(serialConnector.getNewSerialPort(Mockito.anyString())).thenReturn(new SerialPortHardwareMock("COM1"));

        serialPortListWrapper = mock(SerialPortListWrapper.class);
        when(serialPortListWrapper.getPortNames()).thenReturn(new ArrayList<>(Collections.singletonList("COM1")));

        serialConnectionListeners = new SerialConnectionListeners();

        serialPacketTransceiver = new SerialPacketTransceiver();
        serialPacketTransceiver.setConfigController(serialConfigController);
        serialPacketTransceiver.setSerialConnector(serialConnector);
        serialPacketTransceiver.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPacketTransceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketTransceiver.init();

        serialPortListener = new SerialPortListener();
        serialPortListener.setSerialPortList(serialPortListWrapper);
        serialPortListener.setSerialConnector(serialConnector);
        serialPortListener.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPortListener.setSerialConnectionListeners(serialConnectionListeners);
        serialPortListener.setSerialPacketTransceiver(serialPacketTransceiver);
        serialPortListener.setConfigController(serialConfigController);
        serialPortListener.init();

        serialTimeoutService.setSerialPortListener(serialPortListener);
        serialTimeoutService.setSerialConnectionListeners(serialConnectionListeners);
        serialTimeoutService.setSerialPacketTransceiver(serialPacketTransceiver);
        serialTimeoutService.init();
    }

    static class SerialPortHardwareMock extends SerialPortMock {

        public SerialPortHardwareMock(String portName) {
            super(portName);
        }

        @Override
        protected void sendReply(SerialMessageDto serialMessageDto) {
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            byte[] bytes = SerialMessageUtils.getByteArrayFromData(serialMessageDto);

            byte[] buffer = Arrays.copyOfRange(bytes, 0, bytes.length/2);
            readQueue.add(buffer);
            callReadEventListeners(buffer.length);

            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buffer = Arrays.copyOfRange(bytes, bytes.length/2, bytes.length);
            readQueue.add(buffer);
            callReadEventListeners(buffer.length);
        }
    }


    @Test
    public void testFragmentedReply() throws InterruptedException {
        SerialMessageDto pingMessage = serialMessageFactory.getPingMessage();
        AtomicBoolean testCase = new AtomicBoolean(false);
        serialProtocolMessageListeners.addPingReceivedListener(message -> {
            assertEquals(pingMessage.getMessageId(), message.getMessageId());
            assertEquals(pingMessage.getCommandId(), message.getCommandId());
            System.out.println("Test result is " + message);
            testCase.set(true);
        });
        System.out.println("Start");
        serialPortListener.serialScan();
        Thread.sleep(1500);
        serialPortListener.serialScan();
        Thread.sleep(1500);
        System.out.println("Ping");
        serialPacketTransceiver.sendData(pingMessage);
        serialPacketTransceiver.sender();
        Thread.sleep(1500);
        assertTrue(testCase.get());
    }
}
