package com.denvys5.serial.serial_port;

import com.denvys5.serial.service.serial_port.SerialPortMock;
import com.denvys5.serial.service.serial_port.ISerialPort;
import com.denvys5.serial.service.serial_port.SerialException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SerialPortMockTest {

    @Test
    public void isOpened() {
        String com = "COM1";
        ISerialPort serialPort = new SerialPortMock(com);

        if(!serialPort.isOpened()){
            try {
                serialPort.openPort();
            } catch (SerialException e) {
                e.printStackTrace();
            }
            try {
                serialPort.setParams(
                        SerialPortMock.BAUDRATE_9600,
                        SerialPortMock.DATABITS_8,
                        SerialPortMock.STOPBITS_1,
                        SerialPortMock.PARITY_NONE);
            } catch (SerialException e) {
                e.printStackTrace();
            }
        }

        assertTrue(serialPort.isOpened());
    }
}
