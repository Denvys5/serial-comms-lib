package com.denvys5.serial;

import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.service.*;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_listeners.SerialConnectionListeners;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class SerialConnectorTest {
    SerialConnector serialConnector;
    SerialPortListener serialPortListener;
    SerialPortListWrapper serialPortListWrapper;
    SerialPacketTransceiver serialPacketTransceiver;
    SerialPacketReceiver serialPacketReceiver;
    SerialMessageFactory serialMessageFactory;
    SerialProtocolMessageListeners serialProtocolMessageListeners;
    SerialConnectionListeners serialConnectionListeners;
    SerialTimeoutService serialTimeoutService;
    SerialConfigController serialConfigController;

    @BeforeEach
    public void setup(){
        serialMessageFactory = new SerialMessageFactory();

        serialProtocolMessageListeners = new SerialProtocolMessageListeners();

        serialConfigController = spy(SerialConfigController.class);
        when(serialConfigController.isSerialDebug()).thenReturn(true);
        when(serialConfigController.getSerialCommunicationSpeed()).thenReturn(9600);
        when(serialConfigController.isSerialRawMessages()).thenReturn(false);
        when(serialConfigController.getSerialConnectionTimeout()).thenReturn(10000);
        when(serialConfigController.getSerialTimeoutInitialDelay()).thenReturn(25000);
        when(serialConfigController.getSerialClearAcksTimeDelay()).thenReturn(2000);
        when(serialConfigController.getSerialSendMessagesTimeDelay()).thenReturn(100);
        when(serialConfigController.getSerialTimeForACK()).thenReturn(1000);
        when(serialConfigController.getSerialPortScanDelay()).thenReturn(500);
        when(serialConfigController.getSerialWaitForDevice()).thenReturn(1000);
        when(serialConfigController.getSerialWaitForDeviceCycles()).thenReturn(4);

        serialTimeoutService = new SerialTimeoutService();
        serialTimeoutService.setConfigController(serialConfigController);

        serialPacketReceiver = new SerialPacketReceiver();
        serialPacketReceiver.setSerialTimeoutService(serialTimeoutService);
        serialPacketReceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketReceiver.setListeners(serialProtocolMessageListeners);
        serialPacketReceiver.setConfigController(serialConfigController);

        serialConnector = spy(SerialConnector.class);
        serialConnector.setConfigController(serialConfigController);
        serialConnector.setSerialPacketReceiver(serialPacketReceiver);
        serialConnector.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        when(serialConnector.getNewSerialPort(Mockito.anyString())).thenReturn(new SerialPortMockHardwareTest.SerialPortHardwareMock("COM1"));

        serialPortListWrapper = mock(SerialPortListWrapper.class);
        when(serialPortListWrapper.getPortNames()).thenReturn(new ArrayList<>(Collections.singletonList("COM1")));

        serialConnectionListeners = new SerialConnectionListeners();

        serialPacketTransceiver = new SerialPacketTransceiver();
        serialPacketTransceiver.setConfigController(serialConfigController);
        serialPacketTransceiver.setSerialConnector(serialConnector);
        serialPacketTransceiver.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPacketTransceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketTransceiver.init();

        serialPortListener = new SerialPortListener();
        serialPortListener.setSerialPortList(serialPortListWrapper);
        serialPortListener.setSerialConnector(serialConnector);
        serialPortListener.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPortListener.setSerialConnectionListeners(serialConnectionListeners);
        serialPortListener.setSerialPacketTransceiver(serialPacketTransceiver);
        serialPortListener.setConfigController(serialConfigController);
        serialPortListener.init();

        serialTimeoutService.setSerialPortListener(serialPortListener);
        serialTimeoutService.setSerialConnectionListeners(serialConnectionListeners);
        serialTimeoutService.setSerialPacketTransceiver(serialPacketTransceiver);
        serialTimeoutService.init();
    }

    @Test
    public void connect() throws InterruptedException {
        System.out.println("Start");
        serialPortListener.serialScan();
        Thread.sleep(1000);
        serialPortListener.serialScan();
        Thread.sleep(1000);
        System.out.println("Ping");
        serialPacketTransceiver.sendPingRequest();
        serialPacketTransceiver.sender();
        Thread.sleep(5000);
    }

    @Test
    public void testPingCommand() throws InterruptedException {
        SerialMessageDto pingMessage = serialMessageFactory.getPingMessage();
        AtomicBoolean testCase = new AtomicBoolean(false);
        serialProtocolMessageListeners.addPingReceivedListener(message -> {
            assertEquals(pingMessage.getMessageId(), message.getMessageId());
            assertEquals(pingMessage.getCommandId(), message.getCommandId());
            System.out.println("Test result is " + message);
            testCase.set(true);
        });
        System.out.println("Start");
        serialPortListener.serialScan();
        Thread.sleep(1000);
        serialPortListener.serialScan();
        Thread.sleep(1000);
        System.out.println("Ping");
        serialPacketTransceiver.sendData(pingMessage);
        serialPacketTransceiver.sender();
        Thread.sleep(1000);
        assertTrue(testCase.get());
    }
}
