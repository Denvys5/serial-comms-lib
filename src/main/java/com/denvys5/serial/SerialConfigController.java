/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial;

import com.denvys5.config.ConfigFactory;
import com.denvys5.config.KeyValueConfig;


public class SerialConfigController implements ISerialConfigController {
    private KeyValueConfig properties;

    private int serialCommunicationSpeed;
    private int serialTimeForACK;
    private int serialClearAcksTimeDelay;
    private int serialSendMessagesTimeDelay;
    private int serialPortScanDelay;
    private int serialWaitForDevice;
    private int serialWaitForDeviceCycles;
    private int serialConnectionTimeout;
    private int serialTimeoutInitialDelay;
    private boolean serialDebug;
    private boolean serialRawMessages;

    public void serialSetup(){
        properties = ConfigFactory.getPropertiesConfig("system");

        serialDebug = properties.getPropertyBoolean("serial.debug", false);
        serialRawMessages = properties.getPropertyBoolean("serial.rawMessages", false);
        serialCommunicationSpeed = properties.getPropertyInt("serial.communicationSpeed", 9600);
        serialTimeForACK = properties.getPropertyInt("serial.timeForACK", 500);
        serialClearAcksTimeDelay = properties.getPropertyInt("serial.clearAcksTimeDelay", 5000);
        serialSendMessagesTimeDelay = properties.getPropertyInt("serial.sendMessagesTimeDelay", 100);
        serialPortScanDelay = properties.getPropertyInt("serial.portScanDelay", 1000);
        serialWaitForDevice = properties.getPropertyInt("serial.waitForDevice", 2000);
        serialWaitForDeviceCycles = properties.getPropertyInt("serial.waitForDeviceCycles", 4);
        serialConnectionTimeout = properties.getPropertyInt("serial.connectionTimeout", 60000);
        serialTimeoutInitialDelay = properties.getPropertyInt("serial.connectionTimeoutInitialDelay", 25000);
    }

    public boolean isSerialDebug() {
        return serialDebug;
    }

    public boolean isSerialRawMessages() {
        return serialRawMessages;
    }

    @Override
    public int getSerialTimeoutInitialDelay() {
        return serialTimeoutInitialDelay;
    }

    public int getSerialCommunicationSpeed() {
        return serialCommunicationSpeed;
    }

    public int getSerialTimeForACK() {
        return serialTimeForACK;
    }

    public int getSerialClearAcksTimeDelay() {
        return serialClearAcksTimeDelay;
    }

    public int getSerialSendMessagesTimeDelay() {
        return serialSendMessagesTimeDelay;
    }

    public int getSerialWaitForDevice() {
        return serialWaitForDevice;
    }

    public int getSerialWaitForDeviceCycles() {
        return serialWaitForDeviceCycles;
    }

    public int getSerialPortScanDelay() {
        return serialPortScanDelay;
    }

    public int getSerialConnectionTimeout() {
        return serialConnectionTimeout;
    }
}
