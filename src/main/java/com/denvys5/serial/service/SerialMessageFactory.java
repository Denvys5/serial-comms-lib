/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.serial.serial_message.SerialMessage;
import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.serial_message.SerialProtocolCommandEnumeration;

import java.util.Arrays;

public class SerialMessageFactory {
    private byte counter = 0;

    private synchronized byte incrementCounter(){
        if(counter >= 48)
            return ++counter;
        else return counter = 48;
    }

    public SerialMessageDto getSerialMessageDto(byte messageId, byte commandId, byte dataId, byte... argv){
        SerialMessageDto serialMessageDto = new SerialMessageDto(ISerialConstants.MARKER, messageId, commandId, dataId, (byte) argv.length, Arrays.copyOf(argv, argv.length));
        serialMessageDto.setCrc(SerialMessageUtils.getCRCFromIncompleteEntity(serialMessageDto));
        return serialMessageDto;
    }


    public SerialMessageDto getSerialDataMessageDto(byte dataId, byte... argv){
        SerialMessageDto serialMessageDto = new SerialMessageDto(ISerialConstants.MARKER, incrementCounter(), SerialProtocolCommandEnumeration.DATA_COMMAND_ID.getCommandId(), dataId, (byte) argv.length, Arrays.copyOf(argv, argv.length));
        serialMessageDto.setCrc(SerialMessageUtils.getCRCFromIncompleteEntity(serialMessageDto));
        return serialMessageDto;
    }

    public SerialMessageDto getACKMessage(byte messageId){
        return getSerialMessageDto(messageId, SerialProtocolCommandEnumeration.ACK_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE);
    }

    public SerialMessageDto getHandshakeMessage(){
        return getSerialMessageDto(incrementCounter(), SerialProtocolCommandEnumeration.HANDSHAKE_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE);
    }

    public SerialMessageDto getHandshakeMessage(byte messageId){
        return getSerialMessageDto(messageId, SerialProtocolCommandEnumeration.HANDSHAKE_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE);
    }

    public SerialMessageDto getPingMessage(){
        return getSerialMessageDto(incrementCounter(), SerialProtocolCommandEnumeration.PING_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE);
    }

    public SerialMessageDto getPingMessage(byte messageId){
        return getSerialMessageDto(messageId, SerialProtocolCommandEnumeration.PING_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE);
    }

    public SerialMessageDto getMessageToSend(byte commandId, byte dataid, byte... arguments){
        return getSerialMessageDto(incrementCounter(), commandId, dataid, arguments);
    }

    public SerialMessage getSerialMessageFromDto(SerialMessageDto messageDto){
        SerialProtocolCommandEnumeration commandId = SerialProtocolCommandEnumeration.getCommandById(messageDto.getCommandId());
        return new SerialMessage(messageDto.getMessageId(), commandId, messageDto.getDataId(), messageDto.getArgv());
    }

    public SerialMessageDto getMessageFromByteArray(byte[] message){
        return SerialMessageUtils.getDataFromByteArray(message);
    }
}
