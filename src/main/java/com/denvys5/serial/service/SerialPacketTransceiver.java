/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;


import com.denvys5.serial.ISerialConfigController;
import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

public class SerialPacketTransceiver {

    private final Queue<SerialMessageDto> serialMessageDtoQueue = new ConcurrentLinkedQueue<>();
    private final List<TimedACK> timedACKS = new CopyOnWriteArrayList<>();

    private final ScheduledExecutorService messageSenderWorker = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService clearAcksWorker = Executors.newSingleThreadScheduledExecutor();

    private SerialProtocolMessageListeners serialProtocolMessageListeners;
    private SerialConnector serialConnector;
    private SerialMessageFactory serialMessageFactory;
    private ISerialConfigController configController;

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    public void setSerialProtocolMessageListeners(SerialProtocolMessageListeners serialProtocolMessageListeners) {
        this.serialProtocolMessageListeners = serialProtocolMessageListeners;
    }

    public void setSerialConnector(SerialConnector serialConnector) {
        this.serialConnector = serialConnector;
    }

    public void setSerialMessageFactory(SerialMessageFactory serialMessageFactory) {
        this.serialMessageFactory = serialMessageFactory;
    }

    public void init(){
        serialProtocolMessageListeners.addAckPendingListener(this::sendACKForData);
        serialProtocolMessageListeners.addACKReceivedListener(this::receiveACK);

        enableWorkers();
    }

    private void enableWorkers(){
        messageSenderWorker.scheduleAtFixedRate(this::sender, 10000, configController.getSerialSendMessagesTimeDelay(), TimeUnit.MILLISECONDS);
        clearAcksWorker.scheduleAtFixedRate(this::clearACKs, 10000, configController.getSerialClearAcksTimeDelay(), TimeUnit.MILLISECONDS);
    }

    private void addMessageToQueue(SerialMessageDto serialMessageDto){
        serialMessageDtoQueue.add(serialMessageDto);
    }

    boolean received = false;
    public void sender(){
        if(!serialMessageDtoQueue.isEmpty() && serialConnector.isAvailable()){
            SerialMessageDto serialMessageDto = serialMessageDtoQueue.peek();
            serialConnector.send(serialMessageDto);

            try {
                Thread.sleep(configController.getSerialTimeForACK());
            } catch (InterruptedException ignored) {
            }

            received = false;
            for(TimedACK timedACK: timedACKS){
                if(timedACK.getMessage().getMessageId() == serialMessageDto.getMessageId()){
                    timedACKS.remove(timedACK);
                    received = true;
                    break;
                }
            }

            if(received) serialMessageDtoQueue.poll();
        }
    }

    private void clearACKs(){
        long currentTime = System.currentTimeMillis();
        timedACKS.removeIf(timedACK -> currentTime - timedACK.getReceiveTime() > configController.getSerialClearAcksTimeDelay());
    }

    public void sendACKForData(SerialMessageDto dataSerialMessage){
        SerialMessageDto ackMessage = serialMessageFactory.getACKMessage(dataSerialMessage.getMessageId());
        serialConnector.send(ackMessage);
    }

    public void sendHandShakeRequest(){
        SerialMessageDto serialMessageDto = serialMessageFactory.getHandshakeMessage();
        serialConnector.send(serialMessageDto);
    }

    public void sendPingRequest(){
        SerialMessageDto serialMessageDto = serialMessageFactory.getPingMessage();
        addMessageToQueue(serialMessageDto);
    }

    private void receiveACK(SerialMessageDto message){
        timedACKS.add(new TimedACK(message, System.currentTimeMillis()));
    }

    public void sendData(byte[] data){
        addMessageToQueue(serialMessageFactory.getMessageFromByteArray(data));
    }

    public void sendData(SerialMessageDto serialMessageDto){
        addMessageToQueue(serialMessageDto);
    }

    private static class TimedACK {
        private final SerialMessageDto message;
        private final long receiveTime;

        TimedACK(SerialMessageDto message, long receiveTime) {
            this.message = message;
            this.receiveTime = receiveTime;
        }

        SerialMessageDto getMessage() {
            return message;
        }

        long getReceiveTime() {
            return receiveTime;
        }

        @Override
        public String toString() {
            return "TimedACK{" +
                    "message=" + message +
                    ", receiveTime=" + receiveTime +
                    '}';
        }
    }

    public void shutdown(){
        messageSenderWorker.shutdownNow();
        clearAcksWorker.shutdownNow();
    }
}
