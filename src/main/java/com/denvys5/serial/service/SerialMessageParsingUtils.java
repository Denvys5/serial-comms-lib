/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class SerialMessageParsingUtils {
    public static boolean validateByteArrayAsMessage(byte[] array){
        if(array.length < SerialMessageUtils.getSmallestMessageSize())
            return false;
        if(!validateMaskAtArrayStart(array))
            return false;
        byte argc = array[getArgcIndex()];
        return array.length == SerialMessageUtils.getSmallestMessageSize()+(argc & 0xff);
    }

    public static boolean validateMaskAtArrayStart(byte[] array){
        if(array.length < Integer.BYTES) return false;
        int marker = ByteBuffer.wrap(Arrays.copyOf(array, Integer.BYTES)).order(ByteOrder.BIG_ENDIAN).getInt();
        return marker == ISerialConstants.MARKER;
    }

    public static boolean validateCrcInByteArray(byte[] array){
        return SerialMessageUtils.checkCRC(array);
    }

    public static int getArgcIndex(){
        return 7;
    }

    private static final byte firstByteInMarker = (byte) (ISerialConstants.MARKER >> 24);
    private static final byte secondByteInMarker = (byte) (ISerialConstants.MARKER >> 16);
    private static final byte thirdByteInMarker = (byte) (ISerialConstants.MARKER >> 8);
    private static final byte forthByteInMarker = (byte) (ISerialConstants.MARKER);
    public static byte getSelectByteInMarker(int index){
        switch (index){
            case 0: return firstByteInMarker;
            case 1: return secondByteInMarker;
            case 2: return thirdByteInMarker;
            case 3: return forthByteInMarker;
            default: return 0;
        }
    }
}
