/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.serial.ISerialConfigController;
import com.denvys5.serial.service.serial_listeners.SerialConnectionListeners;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SerialTimeoutService {
    private long lastMessageReceived = 0;

    private final ScheduledExecutorService timeoutWorker = Executors.newSingleThreadScheduledExecutor();

    private SerialPacketTransceiver serialPacketTransceiver;
    private SerialPortListener serialPortListener;
    private SerialConnectionListeners serialConnectionListeners;
    private ISerialConfigController configController;

    public void setSerialPacketTransceiver(SerialPacketTransceiver serialPacketTransceiver) {
        this.serialPacketTransceiver = serialPacketTransceiver;
    }

    public void setSerialPortListener(SerialPortListener serialPortListener) {
        this.serialPortListener = serialPortListener;
    }

    public void setSerialConnectionListeners(SerialConnectionListeners serialConnectionListeners) {
        this.serialConnectionListeners = serialConnectionListeners;
    }

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    public void init(){
        timeoutWorker.scheduleAtFixedRate(this::timeoutWorker, configController.getSerialTimeoutInitialDelay(), configController.getSerialConnectionTimeout()/4, TimeUnit.MILLISECONDS);
    }

    public void logSerialMessage() {
        lastMessageReceived = System.currentTimeMillis();
    }

    long currentTime;
    public void timeoutWorker(){
        currentTime = System.currentTimeMillis();
        if(currentTime - lastMessageReceived > configController.getSerialConnectionTimeout()/2){
            serialPacketTransceiver.sendPingRequest();
        }
        if(currentTime > configController.getSerialConnectionTimeout()){
            serialPortListener.disconnect();
            serialConnectionListeners.callTimeoutDisconnectListeners();
            serialPortListener.forceCheckPorts();
        }
    }

    public void shutdown(){
        timeoutWorker.shutdownNow();
    }
}
