/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service.serial_port;

import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.serial_message.SerialProtocolCommandEnumeration;
import com.denvys5.serial.service.ISerialConstants;
import com.denvys5.serial.service.SerialMessageFactory;
import com.denvys5.serial.service.SerialMessageUtils;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class SerialPortMock extends SerialPortImpl {
    protected final ExecutorService executorService;
    protected boolean opened = false;
    protected final String portname;
    protected Queue<byte[]> writeQueue = new ConcurrentLinkedQueue<>();
    protected Queue<byte[]> readQueue = new ConcurrentLinkedQueue<>();
    protected ArrayList<WriteEventListener> writeEventListeners = new ArrayList<>();
    protected ArrayList<ReadEventListener> readEventListeners = new ArrayList<>();
    protected SerialMessageFactory serialMessageFactory = new SerialMessageFactory();

    public SerialPortMock(String portName) {
        super(portName);
        this.portname = portName;
        executorService = Executors.newFixedThreadPool(4);
        writeEventListeners.add(()->{
            parseBytes(writeQueue.poll());
        });
    }

    protected void callWriteEventListeners() {
        for (WriteEventListener listener : writeEventListeners)
            executorService.submit(listener::onEvent);
    }

    protected void callReadEventListeners(int length) {
        for (ReadEventListener listener : readEventListeners)
            executorService.submit(() -> listener.onEvent(length));
    }

    @Override
    public boolean isOpened() {
        log.info("MOCK - isOpened {}", opened);
        return this.opened;
    }

    @Override
    public String getPortName() {
        log.info("MOCK - getPortName {}", portname);
        return portname;
    }

    @Override
    public boolean openPort() {
        log.info("MOCK - openPort");
        this.opened = true;
        return true;
    }

    @Override
    public boolean setParams(int baudRate, int dataBits, int stopBits, int parity) {
        log.info("MOCK - setParams");
        return true;
    }

    @Override
    public boolean closePort() {
        log.info("MOCK - closePort");
        this.opened = false;
        return true;
    }

    @Override
    public boolean writeBytes(byte[] buffer) {
        StringBuilder sb = new StringBuilder();
        for (byte b : buffer) {
            sb.append(String.format("%02X ", b));
        }
        log.info("MOCK - writeBytes {}", sb);

        writeQueue.add(buffer);
        callWriteEventListeners();
        return true;
    }

    protected void parseBytes(byte[] buffer){
        SerialMessageDto serialMessageDto = SerialMessageUtils.getDataFromByteArray(buffer);
        if(SerialMessageUtils.checkCRC(serialMessageDto)){
            SerialMessageDto ack = getAck(serialMessageDto);
            sendReply(ack);

            Optional<SerialMessageDto> reply = getReply(serialMessageDto);
            reply.ifPresent(this::sendReply);
        }
    }

    protected SerialMessageDto getAck(SerialMessageDto serialMessageDto){
        return serialMessageFactory.getACKMessage(serialMessageDto.getMessageId());
    }

    protected Optional<SerialMessageDto> getReply(SerialMessageDto serialMessageDto){
        switch (SerialProtocolCommandEnumeration.getCommandById(serialMessageDto.getCommandId())){
            case PING_COMMAND_ID: {
                return Optional.of(serialMessageFactory.getPingMessage(serialMessageDto.getMessageId()));
            }
            case HANDSHAKE_COMMAND_ID: {
                return Optional.of(serialMessageFactory.getHandshakeMessage(serialMessageDto.getMessageId()));
            }
            case ACK_COMMAND_ID: break;
            case DATA_COMMAND_ID:{
                return Optional.of(serialMessageFactory.getMessageToSend(SerialProtocolCommandEnumeration.DATA_COMMAND_ID.getCommandId(), ISerialConstants.DEFAULT_VALUE));
            }
            default:
                log.info("WTF IS THIS MESSAGE? {}", serialMessageDto);
        }
        return Optional.empty();
    }

    protected void sendReply(SerialMessageDto serialMessageDto){
        try {
            Thread.sleep(25);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        byte[] bytes = SerialMessageUtils.getByteArrayFromData(serialMessageDto);
        readQueue.add(bytes);
        callReadEventListeners(bytes.length);
    }

    @Override
    public void addEventListener(SerialPortEventListener listener) {
        log.info("MOCK - addEventListener");
        readEventListeners.add((length) -> {
            listener.serialEvent(new SerialPortEvent(portname, SerialPortEvent.RXCHAR, length));
        });
    }

    @Override
    public byte[] readBytes() {
        byte[] bytes = readQueue.poll();

        StringBuilder sb = new StringBuilder();
        if (bytes != null) {
            for (byte b : bytes) {
                sb.append(String.format("%02X ", b));
            }
        }

        log.info("MOCK - readBytes {}", sb);
        return bytes;
    }

    private interface WriteEventListener {
        void onEvent();
    }

    private interface ReadEventListener {
        void onEvent(int length);
    }
}
