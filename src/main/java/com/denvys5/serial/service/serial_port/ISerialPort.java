/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service.serial_port;

import jssc.SerialPortEventListener;
import jssc.SerialPortTimeoutException;

import java.io.UnsupportedEncodingException;

public interface ISerialPort {
    int BAUDRATE_110 = 110;
    int BAUDRATE_300 = 300;
    int BAUDRATE_600 = 600;
    int BAUDRATE_1200 = 1200;
    int BAUDRATE_4800 = 4800;
    int BAUDRATE_9600 = 9600;
    int BAUDRATE_14400 = 14400;
    int BAUDRATE_19200 = 19200;
    int BAUDRATE_38400 = 38400;
    int BAUDRATE_57600 = 57600;
    int BAUDRATE_115200 = 115200;
    int BAUDRATE_128000 = 128000;
    int BAUDRATE_256000 = 256000;
    int DATABITS_5 = 5;
    int DATABITS_6 = 6;
    int DATABITS_7 = 7;
    int DATABITS_8 = 8;
    int STOPBITS_1 = 1;
    int STOPBITS_2 = 2;
    int STOPBITS_1_5 = 3;
    int PARITY_NONE = 0;
    int PARITY_ODD = 1;
    int PARITY_EVEN = 2;
    int PARITY_MARK = 3;
    int PARITY_SPACE = 4;
    int PURGE_RXABORT = 2;
    int PURGE_RXCLEAR = 8;
    int PURGE_TXABORT = 1;
    int PURGE_TXCLEAR = 4;
    int MASK_RXCHAR = 1;
    int MASK_RXFLAG = 2;
    int MASK_TXEMPTY = 4;
    int MASK_CTS = 8;
    int MASK_DSR = 16;
    int MASK_RLSD = 32;
    int MASK_BREAK = 64;
    int MASK_ERR = 128;
    int MASK_RING = 256;
    int FLOWCONTROL_NONE = 0;
    int FLOWCONTROL_RTSCTS_IN = 1;
    int FLOWCONTROL_RTSCTS_OUT = 2;
    int FLOWCONTROL_XONXOFF_IN = 4;
    int FLOWCONTROL_XONXOFF_OUT = 8;
    int ERROR_FRAME = 8;
    int ERROR_OVERRUN = 2;
    int ERROR_PARITY = 4;

    String getPortName();
    boolean isOpened();
    boolean openPort() throws SerialException;
    boolean setParams(int baudRate, int dataBits, int stopBits, int parity) throws SerialException;
    boolean setParams(int baudRate, int dataBits, int stopBits, int parity, boolean setRTS, boolean setDTR) throws SerialException;
    boolean purgePort(int flags) throws SerialException;
    boolean setEventsMask(int mask) throws SerialException;
    int getEventsMask() throws SerialException;
    boolean setRTS(boolean enabled) throws SerialException;
    boolean setDTR(boolean enabled) throws SerialException;
    boolean writeBytes(byte[] buffer) throws SerialException;
    boolean writeByte(byte singleByte) throws SerialException;
    boolean writeString(String string) throws SerialException;
    boolean writeString(String string, String charsetName) throws SerialException, UnsupportedEncodingException;
    boolean writeInt(int singleInt) throws SerialException;
    boolean writeIntArray(int[] buffer) throws SerialException;
    byte[] readBytes(int byteCount) throws SerialException;
    String readString(int byteCount) throws SerialException;
    String readHexString(int byteCount) throws SerialException;
    String readHexString(int byteCount, String separator) throws SerialException;
    String[] readHexStringArray(int byteCount) throws SerialException;
    int[] readIntArray(int byteCount) throws SerialException;
    byte[] readBytes(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException;
    String readString(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException;
    String readHexString(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException;
    String readHexString(int byteCount, String separator, int timeout) throws SerialException, SerialPortTimeoutException;
    String[] readHexStringArray(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException;
    int[] readIntArray(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException;
    byte[] readBytes() throws SerialException;
    String readString() throws SerialException;
    String readHexString() throws SerialException;
    String readHexString(String separator) throws SerialException;
    String[] readHexStringArray() throws SerialException;
    int[] readIntArray() throws SerialException;
    int getInputBufferBytesCount() throws SerialException;
    int getOutputBufferBytesCount() throws SerialException;
    boolean setFlowControlMode(int mask) throws SerialException;
    int getFlowControlMode() throws SerialException;
    boolean sendBreak(int duration) throws SerialException;
    int[] getLinesStatus() throws SerialException;
    boolean isCTS() throws SerialException;
    boolean isDSR() throws SerialException;
    boolean isRING() throws SerialException;
    boolean isRLSD() throws SerialException;
    void addEventListener(SerialPortEventListener listener) throws SerialException;
    void addEventListener(SerialPortEventListener listener, int mask) throws SerialException;
    boolean removeEventListener() throws SerialException;
    boolean closePort() throws SerialException;
}
