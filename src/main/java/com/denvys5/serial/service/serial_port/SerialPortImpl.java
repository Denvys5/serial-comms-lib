/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service.serial_port;

import jssc.SerialPort;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import java.io.UnsupportedEncodingException;

public class SerialPortImpl implements ISerialPort{
    private final SerialPort serialPort;
    public SerialPortImpl(String portName) {
        serialPort = new SerialPort(portName);
    }

    @Override
    public String getPortName() {
        return serialPort.getPortName();
    }

    @Override
    public boolean isOpened() {
        return serialPort.isOpened();
    }

    @Override
    public boolean openPort() throws SerialException {
        try {
            return serialPort.openPort();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setParams(int baudRate, int dataBits, int stopBits, int parity) throws SerialException {
        try {
            return serialPort.setParams(baudRate, dataBits, stopBits, parity);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setParams(int baudRate, int dataBits, int stopBits, int parity, boolean setRTS, boolean setDTR) throws SerialException {
        try{
            return serialPort.setParams(baudRate, dataBits, stopBits, parity, setRTS, setDTR);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean purgePort(int flags) throws SerialException {
        try{
            return serialPort.purgePort(flags);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setEventsMask(int mask) throws SerialException {
        try{
            return serialPort.setEventsMask(mask);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int getEventsMask() throws SerialException {
        try{
            return serialPort.getEventsMask();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setRTS(boolean enabled) throws SerialException {
        try{
            return serialPort.setRTS(enabled);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setDTR(boolean enabled) throws SerialException {
        try{
            return serialPort.setDTR(enabled);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeBytes(byte[] buffer) throws SerialException {
        try{
            return serialPort.writeBytes(buffer);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeByte(byte singleByte) throws SerialException {
        try{
            return serialPort.writeByte(singleByte);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeString(String string) throws SerialException {
        try{
            return serialPort.writeString(string);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeString(String string, String charsetName) throws SerialException, UnsupportedEncodingException {
        try{
            return serialPort.writeString(string, charsetName);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeInt(int singleInt) throws SerialException {
        try{
            return serialPort.writeInt(singleInt);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean writeIntArray(int[] buffer) throws SerialException {
        try{
            return serialPort.writeIntArray(buffer);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public byte[] readBytes(int byteCount) throws SerialException {
        try{
            return serialPort.readBytes(byteCount);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readString(int byteCount) throws SerialException {
        try{
            return serialPort.readString(byteCount);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString(int byteCount) throws SerialException {
        try{
            return serialPort.readHexString(byteCount);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString(int byteCount, String separator) throws SerialException {
        try{
            return serialPort.readHexString(byteCount, separator);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String[] readHexStringArray(int byteCount) throws SerialException {
        try{
            return serialPort.readHexStringArray(byteCount);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int[] readIntArray(int byteCount) throws SerialException {
        try{
            return serialPort.readIntArray(byteCount);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public byte[] readBytes(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readBytes(byteCount, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readString(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readString(byteCount, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readHexString(byteCount, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString(int byteCount, String separator, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readHexString(byteCount, separator, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String[] readHexStringArray(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readHexStringArray(byteCount, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int[] readIntArray(int byteCount, int timeout) throws SerialException, SerialPortTimeoutException {
        try{
            return serialPort.readIntArray(byteCount, timeout);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public byte[] readBytes() throws SerialException {
        try{
            return serialPort.readBytes();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readString() throws SerialException {
        try{
            return serialPort.readString();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString() throws SerialException {
        try{
            return serialPort.readHexString();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String readHexString(String separator) throws SerialException {
        try{
            return serialPort.readHexString(separator);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public String[] readHexStringArray() throws SerialException {
        try{
            return serialPort.readHexStringArray();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int[] readIntArray() throws SerialException {
        try{
            return serialPort.readIntArray();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int getInputBufferBytesCount() throws SerialException {
        try{
            return serialPort.getInputBufferBytesCount();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int getOutputBufferBytesCount() throws SerialException {
        try{
            return serialPort.getOutputBufferBytesCount();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean setFlowControlMode(int mask) throws SerialException {
        try{
            return serialPort.setFlowControlMode(mask);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int getFlowControlMode() throws SerialException {
        try{
            return serialPort.getFlowControlMode();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean sendBreak(int duration) throws SerialException {
        try{
            return serialPort.sendBreak(duration);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public int[] getLinesStatus() throws SerialException {
        try{
            return serialPort.getLinesStatus();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean isCTS() throws SerialException {
        try{
            return serialPort.isCTS();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean isDSR() throws SerialException {
        try{
            return serialPort.isDSR();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean isRING() throws SerialException {
        try{
            return serialPort.isRING();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean isRLSD() throws SerialException {
        try{
            return serialPort.isRLSD();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public void addEventListener(SerialPortEventListener listener) throws SerialException {
        try{
            serialPort.addEventListener(listener);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public void addEventListener(SerialPortEventListener listener, int mask) throws SerialException {
        try{
            serialPort.addEventListener(listener, mask);
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean removeEventListener() throws SerialException {
        try{
            return serialPort.removeEventListener();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }

    @Override
    public boolean closePort() throws SerialException {
        try{
            return serialPort.closePort();
        }catch (SerialPortException e){
            throw new SerialException(e);
        }
    }
}
