/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.serial.ISerialConfigController;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_listeners.SerialConnectionListeners;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SerialPortListener {
    private boolean connected = false;
    private boolean connectedAndApproved = false;
    private int counter = 0;
    private List<String> lastPortList;
    private String currentDevice;

    private SerialConnector serialConnector;
    private SerialPacketTransceiver serialPacketTransceiver;
    private SerialConnectionListeners serialConnectionListeners;
    private SerialPortListWrapper serialPortList;
    private SerialProtocolMessageListeners serialProtocolMessageListeners;
    private ISerialConfigController configController;

    private final ScheduledExecutorService portScanWorker = Executors.newSingleThreadScheduledExecutor();

    public void setSerialConnector(SerialConnector serialConnector) {
        this.serialConnector = serialConnector;
    }

    public void setSerialPacketTransceiver(SerialPacketTransceiver serialPacketTransceiver) {
        this.serialPacketTransceiver = serialPacketTransceiver;
    }

    public void setSerialConnectionListeners(SerialConnectionListeners serialConnectionListeners) {
        this.serialConnectionListeners = serialConnectionListeners;
    }

    public void setSerialPortList(SerialPortListWrapper serialPortList) {
        this.serialPortList = serialPortList;
    }

    public void setSerialProtocolMessageListeners(SerialProtocolMessageListeners serialProtocolMessageListeners) {
        this.serialProtocolMessageListeners = serialProtocolMessageListeners;
    }

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    public void init(){
        //TODO rework this at some point
        serialProtocolMessageListeners.addHandShakeReceivedListener(e -> setConnected());
        enableWorkers();
    }

    private void enableWorkers(){
        portScanWorker.scheduleAtFixedRate(this::serialScan, 1000, configController.getSerialPortScanDelay(), TimeUnit.MILLISECONDS);
    }

    public void serialScan() {
        try {
            workcycle();
        } catch (Exception e) {
            log.error("Serial scan failed", e);
        }
    }

    public void forceCheckPorts(){
        lastPortList.clear();
        serialScan();
    }

    void workcycle() {
        if (lastPortList == null) {
            lastPortList = new ArrayList<>();
            lastPortList.addAll(serialPortList.getPortNames());
            if (!lastPortList.isEmpty()) {
                connected = onStartupCheckAllDevices(lastPortList);
                counter = 0;
            }
            return;
        }

        if(configController.isSerialDebug()){
            if(connectedAndApproved) log.info("Currently connected to device {}", currentDevice);
            else log.info("Currently connected serial ports (no device connection): {}", lastPortList.toString());
        }
        try {
            Thread.sleep(configController.getSerialWaitForDevice() / configController.getSerialWaitForDeviceCycles());
        } catch (InterruptedException e) {
            log.warn("Sleep in SerialListener was interrupted");
        }

        List<String> currentPortList = serialPortList.getPortNames();
        if (currentPortList.size() < lastPortList.size()) {
            disconnect();
        }

        if (connectedAndApproved) {
            return;
        }

        if (currentPortList.size() > lastPortList.size() && !connected) {
            if(configController.isSerialDebug()){
                log.info("Currently connected serial ports (more than before): {}", currentPortList);
            }

            ArrayList<String> temp = new ArrayList<>(currentPortList);
            currentPortList.removeAll(lastPortList);
            lastPortList = temp;

            if(configController.isSerialDebug()){
                if(currentPortList.size()!=1)
                    log.warn("Don`t connect devices too fast. Try to connect devices one by one");
            }

            onStartupCheckAllDevices(currentPortList);
        } else {
            lastPortList = currentPortList;
        }

        if (connected && !connectedAndApproved) {
            counter++;
            if (counter > configController.getSerialWaitForDeviceCycles())
                disconnect();
        }
    }

    private void connect(String com){
        if(configController.isSerialDebug()){
            log.info("Trying to connect to {}", com);
        }
        currentDevice = com;
        serialConnector.connect(com);
        serialPacketTransceiver.sendHandShakeRequest();
    }

    public void disconnect(){
        if(Objects.nonNull(serialConnector)){
            serialConnector.disconnect();
        }
        serialConnector.removeInstance();
        connected = false;
        connectedAndApproved = false;
        counter = 0;
        serialConnectionListeners.callDisconnectListeners();
    }

    private void setConnected(){
        connectedAndApproved = true;
        if(configController.isSerialDebug()){
            log.info("Device approved {}", currentDevice);
        }
        serialConnectionListeners.callDeviceConnectListeners();
    }

    private boolean onStartupCheckAllDevices(List<String> deviceList){
        for (int i = deviceList.size()-1; i >= 0; i--) {
            connect(deviceList.get(i));
            try {
                Thread.sleep(configController.getSerialWaitForDevice());
            } catch (InterruptedException ignored) {
            }
            if(!connectedAndApproved)
                disconnect();
            if(connectedAndApproved) return true;
        }
        return false;
    }

    public void shutdown(){
        portScanWorker.shutdownNow();
        disconnect();
    }
}
