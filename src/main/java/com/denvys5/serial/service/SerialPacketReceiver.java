/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.serial.ISerialConfigController;
import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.serial_message.SerialProtocolCommandEnumeration;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_port.ISerialPort;
import com.denvys5.serial.service.serial_port.SerialException;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.ListIterator;

@Slf4j
public class SerialPacketReceiver implements SerialPortEventListener {
    private ISerialPort serialPort;
    private final LinkedList<Byte> readQueue;
    private final byte[] messageIds = new byte[50];
    private int messageIdIterator = 0;

    private SerialProtocolMessageListeners listeners;
    private SerialMessageFactory serialMessageFactory;
    private SerialTimeoutService serialTimeoutService;
    private ISerialConfigController configController;

    public void setListeners(SerialProtocolMessageListeners listeners) {
        this.listeners = listeners;
    }

    public void setSerialMessageFactory(SerialMessageFactory serialMessageFactory) {
        this.serialMessageFactory = serialMessageFactory;
    }

    public void setSerialTimeoutService(SerialTimeoutService serialTimeoutService) {
        this.serialTimeoutService = serialTimeoutService;
    }

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    public SerialPacketReceiver() {
        readQueue = new LinkedList<>();
    }

    void setSerialPort(ISerialPort serialPort) {
        this.serialPort = serialPort;
    }

    void removeDeviceConnection() {
        this.serialPort = null;
        Arrays.fill(this.messageIds, (byte) 0);
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        if(serialPortEvent.isRXCHAR() && serialPortEvent.getEventValue() > 0){
            try {
                byte[] messageBuffer = serialPort.readBytes();
                if(SerialMessageParsingUtils.validateMaskAtArrayStart(messageBuffer)){
                    if(configController.isSerialDebug()){
                        log.debug("It has marker {}", Arrays.toString(messageBuffer));
                    }
                    if(SerialMessageParsingUtils.validateByteArrayAsMessage(messageBuffer)){
                        if(configController.isSerialDebug()){
                            log.debug("This is proper message");
                        }
                        if(SerialMessageParsingUtils.validateCrcInByteArray(messageBuffer)){
                            if(configController.isSerialDebug()){
                                log.debug("With valid crc");
                            }

                            processMessage(messageBuffer);
                        }else{
                            addAllBufferToQueue(messageBuffer);
                        }
                    }else{
                        addAllBufferToQueue(messageBuffer);
                    }
                }else{
                    addAllBufferToQueue(messageBuffer);
                }
                parseQueue();
                if(configController.isSerialRawMessages()){
                    listeners.callByteArrayReceivedListeners(messageBuffer);
                }
            } catch (SerialException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private void processMessage(byte[] messageBuffer) {
        SerialMessageDto serialMessageDto = serialMessageFactory.getMessageFromByteArray(messageBuffer);

        if(configController.isSerialDebug()){
            log.debug("We got a message {}", serialMessageDto.toString());
        }
        serialTimeoutService.logSerialMessage();
        switch (SerialProtocolCommandEnumeration.getCommandById(serialMessageDto.getCommandId())){
            case ACK_COMMAND_ID: {
                onACKReceived(serialMessageDto);
                break;
            }
            case HANDSHAKE_COMMAND_ID: {
                onHandShakeReceived(serialMessageDto);
                break;
            }
            case PING_COMMAND_ID:{
                onPingReceived(serialMessageDto);
                break;
            }
            case DATA_COMMAND_ID:{
                onAckPending(serialMessageDto);
                onDataReceived(serialMessageDto);
                break;
            }
            default: if(configController.isSerialDebug())
                log.error("Unknown command {}", serialMessageDto);
        }
    }

    private void addAllBufferToQueue(byte[] messageBuffer) {
        if(configController.isSerialDebug()){
            log.debug("Adding part to buffer {}", Arrays.toString(messageBuffer));
        }
        synchronized (readQueue) {
            for (byte buffer : messageBuffer)
                readQueue.add(buffer);
        }
    }

    private void parseQueue(){
        synchronized (readQueue){
            int startIndex = readQueue.indexOf(SerialMessageParsingUtils.getSelectByteInMarker(0));
            if(startIndex == -1) return;

            boolean foundProperlySizedMessage = false;
            boolean foundInvalidMessage = false;
            int arraySize = SerialMessageUtils.getSmallestMessageSize();
            if(checkMarkerByIndex(startIndex)){
                if(readQueue.size() >= startIndex + SerialMessageUtils.getSmallestMessageSize()){
                    int argc = readQueue.get(SerialMessageParsingUtils.getArgcIndex()) & 0xFF;
                    arraySize = SerialMessageUtils.getSmallestMessageSize() + argc;
                    if(readQueue.size() >= startIndex + arraySize){
                        foundProperlySizedMessage = true;
                        ListIterator<Byte> iterator = readQueue.listIterator(startIndex);
                        byte[] buffer = new byte[arraySize];
                        for(int i = 0; i < buffer.length; i++){
                            buffer[i] = iterator.next();
                        }

                        if(parseByteRange(buffer)){
                            processMessage(buffer);
                        }else foundInvalidMessage = true;
                    }
                }
            }

            for(int i = 0; i < startIndex; i++){
                readQueue.removeFirst();
            }

            if(foundInvalidMessage){
                for(int i = 0; i < Integer.BYTES; i++){
                    readQueue.removeFirst();
                }
            }else{
                if(foundProperlySizedMessage){
                    if(readQueue.size() >= arraySize){
                        for(int i = 0; i < arraySize; i++){
                            readQueue.removeFirst();
                        }
                    }else{
                        readQueue.clear();
                    }
                }
            }
        }
    }

    private boolean parseByteRange(byte[] array){
        if(SerialMessageParsingUtils.validateMaskAtArrayStart(array)){
            if(configController.isSerialDebug()){
                log.debug("It has marker");
            }
            if(SerialMessageParsingUtils.validateByteArrayAsMessage(array)){
                if(configController.isSerialDebug()){
                    log.debug("This is proper message");
                }
                if(SerialMessageParsingUtils.validateCrcInByteArray(array)){
                    if(configController.isSerialDebug()){
                        log.debug("With valid crc");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    //TODO rework this to array compare
    private boolean checkMarkerByIndex(int index){
        ListIterator<Byte> iterator = readQueue.listIterator(index+1);
        if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(1))){
            if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(2))){
                if(iterator.next().equals(SerialMessageParsingUtils.getSelectByteInMarker(3))){
                    return true;
                }
            }
        }
        return false;
    }

    private void onAckPending(SerialMessageDto serialMessageDto){
        listeners.callAckPendingListeners(serialMessageDto);
    }

    private void onDataReceived(SerialMessageDto serialMessageDto) {
        if(isUniqueMessage(serialMessageDto.getMessageId())){
            listeners.callDataReceivedListeners(serialMessageFactory.getSerialMessageFromDto(serialMessageDto));
        }else{
            if(configController.isSerialDebug()){
                log.debug("Skipping duplicate message");
            }
        }
    }

    private void onACKReceived(SerialMessageDto message){
        listeners.callACKReceivedListeners(message);
    }

    private void onHandShakeReceived(SerialMessageDto message){
        if(isUniqueMessage(message.getMessageId())){
            listeners.callHandShakeReceivedListeners(message);
        }else{
            if(configController.isSerialDebug()){
                log.debug("Skipping duplicate message");
            }
        }
    }

    private synchronized boolean isUniqueMessage(byte newMessageId){
        for (byte messageId : messageIds) {
            if (messageId == newMessageId) {
                return false;
            }
        }
        messageIds[messageIdIterator++] = newMessageId;
        if(messageIdIterator == messageIds.length)
            messageIdIterator = 0;
        return true;
    }

    private void onPingReceived(SerialMessageDto message){
        listeners.callPingReceivedListeners(message);
    }

    public void shutdown(){
        removeDeviceConnection();
    }
}
