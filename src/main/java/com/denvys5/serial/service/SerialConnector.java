/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.serial.ISerialConfigController;
import com.denvys5.serial.serial_message.SerialMessageDto;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_port.ISerialPort;
import com.denvys5.serial.service.serial_port.SerialException;
import com.denvys5.serial.service.serial_port.SerialPortImpl;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Objects;

@Slf4j
public class SerialConnector {
    private ISerialPort serialPort;
    private SerialPacketReceiver serialPacketReceiver;
    private SerialProtocolMessageListeners serialProtocolMessageListeners;
    private ISerialConfigController configController;

    private String device;

    public SerialConnector() {
    }

    public void setSerialProtocolMessageListeners(SerialProtocolMessageListeners serialProtocolMessageListeners) {
        this.serialProtocolMessageListeners = serialProtocolMessageListeners;
    }

    public void setSerialPacketReceiver(SerialPacketReceiver serialPacketReceiver) {
        this.serialPacketReceiver = serialPacketReceiver;
    }

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    boolean isAvailable(){
        return Objects.nonNull(serialPort) && serialPort.isOpened();
    }

    void connect(String com){
        serialPort = getNewSerialPort(com);

        if(!serialPort.isOpened()){
            try {
                serialPort.openPort();
            } catch (SerialException e) {
                log.error("Failed to open serial port", e);
            }
            try {
                serialPort.setParams(configController.getSerialCommunicationSpeed(),
                        SerialPortImpl.DATABITS_8,
                        SerialPortImpl.STOPBITS_1,
                        SerialPortImpl.PARITY_NONE);
            } catch (SerialException e) {
                log.error("Failed to configure serial port", e);
            }
            try {
                serialPacketReceiver.setSerialPort(serialPort);
                serialPort.addEventListener(serialPacketReceiver);
                Thread.sleep(1000);
            } catch (SerialException | InterruptedException e) {
                log.error("Failed to start serial port", e);
            }

        }

        device = com;
    }

    public ISerialPort getNewSerialPort(String com){
        return new SerialPortImpl(com);
    }

    void removeInstance(){
        serialPort = null;
        serialPacketReceiver.removeDeviceConnection();
        device = null;
    }

    void disconnect() {
        if(Objects.nonNull(serialPort)){
            if(serialPort.isOpened()){
                try {
                    serialPort.closePort();
                } catch (SerialException e) {
                    log.warn("Device {} has disconnected itself", device);
                }
            }
        }else{
            log.warn("Device {} has disconnected itself", device);
        }
    }

    void send(SerialMessageDto serialMessageDto){
        if(Objects.isNull(serialPort)){
            if(configController.isSerialDebug()){
                log.info("Trying to send message {} but there is no connection", serialMessageDto);
            }
            return;
        }
        if(serialPort.isOpened()){
            if(configController.isSerialDebug()){
                log.info("We send into {} message {}", device, serialMessageDto.toString());
            }
            try {
                if(configController.isSerialRawMessages()){
                    byte[] data = SerialMessageUtils.getByteArrayFromData(serialMessageDto);
                    serialPort.writeBytes(data);
                    serialProtocolMessageListeners.callByteArraySentListeners(data);
                }else{
                    serialPort.writeBytes(SerialMessageUtils.getByteArrayFromData(serialMessageDto));
                }
            } catch (SerialException e) {
                log.error("Failed to write data to serial port", e);
            }
        }
    }

    void send(byte[] data){
        if(Objects.isNull(serialPort)){
            if(configController.isSerialDebug()){
                log.info("Trying to send message {} but there is no connection", Arrays.toString(data));
            }
            return;
        }
        if(serialPort.isOpened()){
            if(configController.isSerialDebug()){
                log.info("We send into {} message {}", device, Arrays.toString(data));
            }
            try {
                serialPort.writeBytes(data);
                if(configController.isSerialRawMessages()){
                    serialProtocolMessageListeners.callByteArraySentListeners(data);
                }
            } catch (SerialException e) {
                log.error("Failed to write data to serial port", e);
            }
        }
    }

}
