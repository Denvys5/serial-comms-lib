/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service.serial_listeners;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SerialConnectionListeners {
    private final ExecutorService executorService;

    public SerialConnectionListeners() {
        executorService = Executors.newCachedThreadPool();
    }

    private final ArrayList<SerialEventListener> deviceConnectListeners = new ArrayList<>();

    public ArrayList<SerialEventListener> getDeviceConnectListeners() {
        return deviceConnectListeners;
    }

    public void addDeviceConnectListener(SerialEventListener listener) {
        deviceConnectListeners.add(listener);
    }

    public void callDeviceConnectListeners() {
        for (SerialEventListener listener : deviceConnectListeners)
            executorService.submit(() -> listener.onEvent());
    }

    public void removeDeviceConnectListener(SerialEventListener listener) {
        deviceConnectListeners.remove(listener);
    }

    private final ArrayList<SerialEventListener> deviceDisconnectListeners = new ArrayList<>();

    public ArrayList<SerialEventListener> getDisconnectListeners() {
        return deviceDisconnectListeners;
    }

    public void addDisconnectListener(SerialEventListener listener) {
        deviceDisconnectListeners.add(listener);
    }

    public void callDisconnectListeners() {
        for (SerialEventListener listener : deviceDisconnectListeners)
            executorService.submit(() -> listener.onEvent());
    }

    public void removeDisconnectListener(SerialEventListener listener) {
        deviceDisconnectListeners.remove(listener);
    }

    private final ArrayList<SerialEventListener> timeoutDisconnectListeners = new ArrayList<>();

    public ArrayList<SerialEventListener> getTimeoutDisconnectListeners() {
        return timeoutDisconnectListeners;
    }

    public void addTimeoutDisconnectListener(SerialEventListener listener) {
        timeoutDisconnectListeners.add(listener);
    }

    public void callTimeoutDisconnectListeners() {
        for (SerialEventListener listener : timeoutDisconnectListeners)
            executorService.submit(() -> listener.onEvent());
    }

    public void removeTimeoutDisconnectListener(SerialEventListener listener) {
        timeoutDisconnectListeners.remove(listener);
    }

    public void shutdown(){
        executorService.shutdownNow();
    }
}
