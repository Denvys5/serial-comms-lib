/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service;

import com.denvys5.crypto.Crc16;
import com.denvys5.serial.serial_message.SerialMessageDto;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SerialMessageUtils {
    private static final Crc16 crc = new Crc16((short) 0, (short) 0x1021);
    private static final int smallestMessageSize = new SerialMessageDto((byte)0,(byte)0,(byte)0,(byte)0,(byte)0,new byte[0],(short)0).getTotalLength();
    public static int getSmallestMessageSize(){
        return smallestMessageSize;
    }

    public static byte[] getByteArrayFromData(SerialMessageDto data){
        byte[] result = new byte[data.getTotalLength()];

        result[0] = (byte) (data.getMarker() >> 24);
        result[1] = (byte) (data.getMarker() >> 16);
        result[2] = (byte) (data.getMarker() >> 8);
        result[3] = (byte) data.getMarker();
        result[4] = data.getMessageId();
        result[5] = data.getCommandId();
        result[6] = data.getDataId();
        result[7] = data.getArgc();
        if(data.getArgc() != 0 && data.getArgv().length == data.getIntArgc()){
            System.arraycopy(data.getArgv(), 0, result, 8, data.getIntArgc());
            result[8 + data.getIntArgc()] = (byte) (data.getCrc() >> 8);
            result[9 + data.getIntArgc()] = (byte) data.getCrc();
        }else{
            result[8] = (byte) (data.getCrc() >> 8);
            result[9] = (byte) data.getCrc();
        }

        return result;
    }

    public static SerialMessageDto getDataFromByteArray(byte[] data){
        byte[] marker = new byte[4];
        byte argc = data[7];
        byte[] argv = new byte[argc & 0xFF];
        byte[] crc = new byte[2];
        System.arraycopy(data, 0, marker, 0, marker.length);
        System.arraycopy(data, 8, argv, 0, argv.length);
        System.arraycopy(data, 8 + argv.length, crc, 0, crc.length);
        return new SerialMessageDto(
                ByteBuffer.wrap(marker).order(ByteOrder.BIG_ENDIAN).getInt(),
                data[4],
                data[5],
                data[6],
                argc,
                argv,
                ByteBuffer.wrap(crc).order(ByteOrder.BIG_ENDIAN).getShort()
        );
    }

    public static boolean checkCRC(byte[] data){
        //data has last 2 elements as CRC16
        short crcData = ByteBuffer.wrap(data, data.length-2, 2).order(ByteOrder.BIG_ENDIAN).getShort();
        short crcResult = crc.getCRC(data, 0, data.length-2);
        return crcResult == crcData;
    }

    public static boolean checkCRC(SerialMessageDto data){
        return checkCRC(getByteArrayFromData(data));
    }

    public static short getCRC(byte[] data){
        //data has last 2 elements as CRC16
        return crc.getCRC(data, 0, data.length-2);
    }

    public static short getCRCFromIncompleteEntity(SerialMessageDto data){
        return getCRC(getByteArrayFromData(data));
    }

    public static SerialMessageDto getReply(SerialMessageDto serialMessageDto, byte[] argv){
        SerialMessageDto reply = new SerialMessageDto(
                serialMessageDto.getMarker(),
                (byte)(serialMessageDto.getMessageId()+1),
                serialMessageDto.getCommandId(),
                serialMessageDto.getDataId(),
                (byte)argv.length,
                argv,
                (byte)0
        );
        reply.setCrc(getCRCFromIncompleteEntity(reply));
        return reply;
    }
}
