/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial.service.message_listeners;

import com.denvys5.serial.serial_message.SerialMessage;
import com.denvys5.serial.serial_message.SerialMessageDto;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SerialProtocolMessageListeners {
    private final ExecutorService executorService;

    public SerialProtocolMessageListeners() {
        executorService = Executors.newCachedThreadPool();
    }

    private final ArrayList<SerialByteArrayEventListener> byteArrayReceivedListeners = new ArrayList<>();

    public ArrayList<SerialByteArrayEventListener> getByteArrayReceivedListeners() {
        return byteArrayReceivedListeners;
    }

    public void addByteArrayReceivedListener(SerialByteArrayEventListener listener) {
        byteArrayReceivedListeners.add(listener);
    }

    public void callByteArrayReceivedListeners(byte[] message) {
        for (SerialByteArrayEventListener listener : byteArrayReceivedListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removeByteArrayReceivedListener(SerialByteArrayEventListener listener) {
        byteArrayReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialByteArrayEventListener> byteArraySentListeners = new ArrayList<>();

    public ArrayList<SerialByteArrayEventListener> getByteArraySentListeners() {
        return byteArraySentListeners;
    }

    public void addByteArraySentListener(SerialByteArrayEventListener listener) {
        byteArraySentListeners.add(listener);
    }

    public void callByteArraySentListeners(byte[] message) {
        for (SerialByteArrayEventListener listener : byteArraySentListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removeByteArraySentListener(SerialByteArrayEventListener listener) {
        byteArraySentListeners.remove(listener);
    }

    private final ArrayList<SerialMessageReceivedListener> messageReceivedListeners = new ArrayList<>();

    public ArrayList<SerialMessageReceivedListener> getMessageReceivedListeners() {
        return messageReceivedListeners;
    }

    public void addMessageReceivedListener(SerialMessageReceivedListener listener) {
        messageReceivedListeners.add(listener);
    }

    public void callMessageReceivedListeners(SerialMessageDto serialMessageDto) {
        for (SerialMessageReceivedListener listener : messageReceivedListeners)
            executorService.submit(() -> listener.onReceive(serialMessageDto));
    }

    public void removeMessageReceivedListener(SerialMessageReceivedListener listener) {
        messageReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialMessageReceivedListener> handShakeReceivedListeners = new ArrayList<>();

    public ArrayList<SerialMessageReceivedListener> getHandShakeReceivedListeners() {
        return handShakeReceivedListeners;
    }

    public void addHandShakeReceivedListener(SerialMessageReceivedListener listener) {
        handShakeReceivedListeners.add(listener);
    }

    public void callHandShakeReceivedListeners(SerialMessageDto message) {
        for (SerialMessageReceivedListener listener : handShakeReceivedListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removeHandShakeReceivedListener(SerialMessageReceivedListener listener) {
        handShakeReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialMessageReceivedListener> ackReceivedListeners = new ArrayList<>();

    public ArrayList<SerialMessageReceivedListener> getACKReceivedListeners() {
        return ackReceivedListeners;
    }

    public void addACKReceivedListener(SerialMessageReceivedListener listener) {
        ackReceivedListeners.add(listener);
    }

    public void callACKReceivedListeners(SerialMessageDto message) {
        for (SerialMessageReceivedListener listener : ackReceivedListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removeACKReceivedListener(SerialMessageReceivedListener listener) {
        ackReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialMessageReceivedListener> pingReceivedListeners = new ArrayList<>();

    public ArrayList<SerialMessageReceivedListener> getPingReceivedListeners() {
        return pingReceivedListeners;
    }

    public void addPingReceivedListener(SerialMessageReceivedListener listener) {
        pingReceivedListeners.add(listener);
    }

    public void callPingReceivedListeners(SerialMessageDto message) {
        for (SerialMessageReceivedListener listener : pingReceivedListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removePingReceivedListener(SerialMessageReceivedListener listener) {
        pingReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialDataReceivedListener> dataReceivedListeners = new ArrayList<>();

    public ArrayList<SerialDataReceivedListener> getDataReceivedListeners() {
        return dataReceivedListeners;
    }

    public void addDataReceivedListener(SerialDataReceivedListener listener) {
        dataReceivedListeners.add(listener);
    }

    public void callDataReceivedListeners(SerialMessage message) {
        for (SerialDataReceivedListener listener : dataReceivedListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removeDataReceivedListener(SerialDataReceivedListener listener) {
        dataReceivedListeners.remove(listener);
    }

    private final ArrayList<SerialMessageReceivedListener> ackPendingListeners = new ArrayList<>();

    public ArrayList<SerialMessageReceivedListener> getAckPendingListeners() {
        return ackPendingListeners;
    }

    public void addAckPendingListener(SerialMessageReceivedListener listener) {
        ackPendingListeners.add(listener);
    }

    public void callAckPendingListeners(SerialMessageDto message) {
        for (SerialMessageReceivedListener listener : ackPendingListeners)
            executorService.submit(() -> listener.onReceive(message));
    }

    public void removevListener(SerialMessageReceivedListener listener) {
        ackPendingListeners.remove(listener);
    }

    public void shutdown(){
        executorService.shutdownNow();
    }
}
