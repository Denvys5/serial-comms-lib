/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Denvys5
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.serial;

import com.denvys5.serial.service.*;
import com.denvys5.serial.service.message_listeners.SerialProtocolMessageListeners;
import com.denvys5.serial.service.serial_listeners.SerialConnectionListeners;

import java.util.Objects;

public class SerialController {
    private ISerialConfigController configController;
    private SerialPacketTransceiver serialPacketTransceiver;
    private SerialProtocolMessageListeners serialProtocolMessageListeners;
    private SerialConnectionListeners serialConnectionListeners;
    private SerialPortListener serialPortListener;
    private SerialConnector serialConnector;
    private SerialPacketReceiver serialPacketReceiver;
    private SerialMessageProcessor serialMessageProcessor;
    private SerialMessageFactory serialMessageFactory;
    private SerialPortListWrapper serialPortList;
    private SerialTimeoutService serialTimeoutService;

    public void init(){
        if(Objects.isNull(configController))
            configController = new SerialConfigController();
        configController.serialSetup();

        if(Objects.isNull(serialProtocolMessageListeners))
            serialProtocolMessageListeners = new SerialProtocolMessageListeners();

        if(Objects.isNull(serialConnectionListeners))
            serialConnectionListeners = new SerialConnectionListeners();

        if(Objects.isNull(serialPortList))
            serialPortList = new SerialPortListWrapper();

        if(Objects.isNull(serialMessageFactory))
            serialMessageFactory = new SerialMessageFactory();

        if(Objects.isNull(serialTimeoutService))
            serialTimeoutService = new SerialTimeoutService();
        serialTimeoutService.setConfigController(configController);
        serialTimeoutService.setSerialConnectionListeners(serialConnectionListeners);
        serialTimeoutService.setSerialPortListener(serialPortListener);

        if(Objects.isNull(serialPacketReceiver))
            serialPacketReceiver = new SerialPacketReceiver();
        serialPacketReceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketReceiver.setListeners(serialProtocolMessageListeners);
        serialPacketReceiver.setSerialTimeoutService(serialTimeoutService);
        serialPacketReceiver.setConfigController(configController);

        if(Objects.isNull(serialConnector))
            serialConnector = new SerialConnector();
        serialConnector.setConfigController(configController);
        serialConnector.setSerialPacketReceiver(serialPacketReceiver);
        serialConnector.setSerialProtocolMessageListeners(serialProtocolMessageListeners);

        if(Objects.isNull(serialMessageProcessor))
            serialMessageProcessor = new SerialMessageProcessor();

        if(Objects.isNull(serialPacketTransceiver))
            serialPacketTransceiver = new SerialPacketTransceiver();
        serialPacketTransceiver.setConfigController(configController);
        serialPacketTransceiver.setSerialConnector(serialConnector);
        serialPacketTransceiver.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPacketTransceiver.setSerialMessageFactory(serialMessageFactory);
        serialPacketTransceiver.init();

        if(Objects.isNull(serialPortListener))
            serialPortListener = new SerialPortListener();
        serialPortListener.setConfigController(configController);
        serialPortListener.setSerialPortList(serialPortList);
        serialPortListener.setSerialConnector(serialConnector);
        serialPortListener.setSerialProtocolMessageListeners(serialProtocolMessageListeners);
        serialPortListener.setSerialConnectionListeners(serialConnectionListeners);
        serialPortListener.setSerialPacketTransceiver(serialPacketTransceiver);
        serialPortListener.init();

        serialTimeoutService.setSerialPacketTransceiver(serialPacketTransceiver);
        serialTimeoutService.init();

        serialProtocolMessageListeners.addDataReceivedListener(message -> {
            serialMessageProcessor.processDataMessage(message);
        });
    }

    public void setConfigController(ISerialConfigController configController) {
        this.configController = configController;
    }

    public void setSerialPacketTransceiver(SerialPacketTransceiver serialPacketTransceiver) {
        this.serialPacketTransceiver = serialPacketTransceiver;
    }

    public void setSerialConnectionListeners(SerialConnectionListeners serialConnectionListeners) {
        this.serialConnectionListeners = serialConnectionListeners;
    }

    public void setSerialPortListener(SerialPortListener serialPortListener) {
        this.serialPortListener = serialPortListener;
    }

    public void setSerialConnector(SerialConnector serialConnector) {
        this.serialConnector = serialConnector;
    }

    public void setSerialPacketReceiver(SerialPacketReceiver serialPacketReceiver) {
        this.serialPacketReceiver = serialPacketReceiver;
    }

    public void setSerialMessageProcessor(SerialMessageProcessor serialMessageProcessor) {
        this.serialMessageProcessor = serialMessageProcessor;
    }

    public void setSerialMessageFactory(SerialMessageFactory serialMessageFactory) {
        this.serialMessageFactory = serialMessageFactory;
    }

    public void setSerialPortList(SerialPortListWrapper serialPortList) {
        this.serialPortList = serialPortList;
    }

    public void setSerialTimeoutService(SerialTimeoutService serialTimeoutService) {
        this.serialTimeoutService = serialTimeoutService;
    }

    public void setSerialProtocolMessageListeners(SerialProtocolMessageListeners serialProtocolMessageListeners) {
        this.serialProtocolMessageListeners = serialProtocolMessageListeners;
    }

    public SerialPacketTransceiver getSerialPacketTransceiver() {
        return serialPacketTransceiver;
    }

    public SerialConnectionListeners getSerialConnectionListeners() {
        return serialConnectionListeners;
    }

    public SerialProtocolMessageListeners getSerialListeners() {
        return serialProtocolMessageListeners;
    }

    public SerialConnector getSerialConnector() {
        return serialConnector;
    }

    public SerialPacketReceiver getSerialPacketReceiver() {
        return serialPacketReceiver;
    }

    public SerialMessageProcessor getSerialMessageProcessor() {
        return serialMessageProcessor;
    }

    public SerialMessageFactory getSerialMessageFactory() {
        return serialMessageFactory;
    }

    public SerialPortListener getSerialPortListener() {
        return serialPortListener;
    }

    public SerialPortListWrapper getSerialPortList() {
        return serialPortList;
    }

    public SerialProtocolMessageListeners getSerialMessageListeners() {
        return serialProtocolMessageListeners;
    }

    public SerialTimeoutService getSerialTimeoutService() {
        return serialTimeoutService;
    }

    public ISerialConfigController getConfigController() {
        return configController;
    }

    public SerialProtocolMessageListeners getSerialProtocolMessageListeners() {
        return serialProtocolMessageListeners;
    }

    public void shutdown(){
        serialTimeoutService.shutdown();
        serialConnectionListeners.shutdown();
        serialProtocolMessageListeners.shutdown();
        serialPacketReceiver.shutdown();
        serialPacketTransceiver.shutdown();
        serialPortListener.shutdown();
    }
}
