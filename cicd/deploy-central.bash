#!/bin/bash

dnf swap gnupg2-minimal gnupg2-full -y

gpg --batch --import $CI_PROJECT_DIR/../$CI_PROJECT_NAME.tmp/GPG_PUBLIC_KEY
gpg --batch --passphrase ${GPG_PASSPHRASE} --import $CI_PROJECT_DIR/../$CI_PROJECT_NAME.tmp/GPG_PRIVATE_KEY

export MAVEN_GPG_PASSPHRASE=$GPG_PASSPHRASE
mvn $MAVEN_CLI_OPTS -DskipTests -P maven-central-release deploy
